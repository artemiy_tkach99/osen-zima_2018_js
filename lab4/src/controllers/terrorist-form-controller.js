var { view } = require('../view-engine/view-engine');

let terroristFormController = (req, res) => {
    view(req, res, 'addForm');
}

exports.terroristFormController = terroristFormController;