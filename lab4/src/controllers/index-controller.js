var {view} = require('../view-engine/view-engine');

let indexController = (req, res) => {
    console.log(req.dataFromMiddleware)
    view(req, res, 'index');
}

exports.indexController = indexController;
