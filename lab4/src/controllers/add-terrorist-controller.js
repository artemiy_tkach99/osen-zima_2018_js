var {Terrorist} = require('../models/terrorist');
qs = require('querystring');
var {sendRedirect} = require('../helpers/sendRedirect');

let addTerroristController = (req, res) => {
    var body = '';
    req.on('data', function (data) {
        body += data;
    });
    req.on('end', function () {

        var post = qs.parse(body);
        Terrorist.addTerrorist({
            name: post['name'],
            health: post['health'],
            damage: post['damage'],
            isGoodTerrorist: post['isGoodTerrorist']
        })
        sendRedirect(res, '/terrorists');
    });
}

exports.addTerroristController = addTerroristController;