var express = require('express');
var router = express.Router();
var controllers = require('../controllers');

router.get('/', controllers.indexController);
router.all('/terrorists', controllers.addTerrorist);

module.exports.router = router;