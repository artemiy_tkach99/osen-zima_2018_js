var {Terrorist} = require('../models/terrorist');

module.exports.indexController = (req, res) => {

	Terrorist.getAll()
	.then(data => {
		res.render('home', {terrorists: data});
	});
}

//controllers/index.js
