var {Terrorist} = require('../models/terrorist');

module.exports.addTerrorist = (req, res) => {
    if(req.body.name){
        Terrorist.addTerrorist({
            name: req.body.name,
            health: req.body.health,
            damage: req.body.damage,
            isGoodTerrorist: req.body.isGoodTerrorist
        })
        .then(res => {
            res.redirct('/');
        });
    }
    
    res.render('add');
}