var fs = require('fs');

exports.Terrorist = class Terrorist{
    constructor(name, health, damage, isGoodTerrorist)
    {
        this.name = name;
        this.health = health;
        this.damage = damage;
        this.isGoodTerrorist = isGoodTerrorist;
    }

    static getAll(){
        return new Promise(resolve => {
            fs.readFile('models/terrorists.json', (err, data) => {
                let terrorists = JSON.parse(data);
                resolve(terrorists);
            });
        });
    }

    static addTerrorist(terrorist){
        return new Promise(resolve => {
            this.getAll()
            .then(data => {
                data.push(terrorist);
                let res = JSON.stringify(data);

                fs.writeFile('models/terrorists.json', res, (err) => {
                    resolve({});
                });
            })
        });
    }
}