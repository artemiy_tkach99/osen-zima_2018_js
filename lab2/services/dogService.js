const {Dog} = require('../models/dog');

let dogsExist = true;

exports.doIGetADog = function doIGetADog(){
	return new Promise((resolve, reject) => {
		if (dogsExist) {
			let dogs = [
				new Dog('Watashiwa', 1),
				new Dog('Yakusokuwo', 2),
				new Dog('Wakarimasen', 3)
			];
			resolve(dogs);
		} else {
			reject(new Error('There are no dogs, my man.'));
		}
	});
}