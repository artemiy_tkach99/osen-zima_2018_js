const {Animal} = require('../models/animal')
 
exports.Dog = class Dog extends Animal{
    constructor(name, age){
        super(name);
        this.age = age;
    }
}