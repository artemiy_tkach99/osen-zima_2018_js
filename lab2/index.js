const {doIGetADog} = require('../lab2/services/dogService');

function getDogs() {
	doIGetADog()
	.then((dogs) => {
		return new Promise((resolve, reject) => {
			if (dogs.length > 0) {
				resolve(dogs);
			} else {
				reject(new Error('Error. There needs to be at least one dog.'));
			}
		});
	})
	.then((dogs) => {
		dogs.forEach(dog => {
			console.log(dog);
		});
	})
	.catch((err) => {
		console.log(err.message)
	})
}

getDogs();
