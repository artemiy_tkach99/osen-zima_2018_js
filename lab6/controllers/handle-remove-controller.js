var {Terrorist} = require('../models');

module.exports.handleRemoveController = (req, res) => {
    let id = req.params.id;

    Terrorist.findById(id)
        .then(data => {
            return data.destroy();
        })
        .then(() => {
            res.redirect('/terrorists');
        });
    
}