var { Terrorist } = require('../models');

module.exports.handleAddController = (req, res) => {
    let name = req.body.name;

    Terrorist.create({
        name: name
    })
        .then(terr => {
            res.redirect('/terrorists');
        });
}