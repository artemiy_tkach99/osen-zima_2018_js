// Створити маршрут /, за яким node.js зчитає index.html з диску і поверне користуваченві.
// Створити маршрут /todo/add який міститиме форму для додавання todo(метод POST).
// Створити маршрут /todos/, який дописуватиме передану todo в кінець файлу todo.txt.
// Якщо маршрут не було знайдено відобразити 404.

var http = require('http');
var fs = require('fs');
var multiparty = require('multiparty');

let handler = (req, res) => {
    if(req.url === '/' && req.method === 'GET'){
        fs.readFile('./lab3/index.html', (err, data) => {
            if(err){
                res.end('Error loading html file');
                return;
            }
            res.end(data);
        });
        return;
    }

    if(req.url === '/todo/add' && req.method === 'GET'){
        fs.readFile('./lab3/addtodo.html', (err, data) => {
            if(err){
                res.end('Error loading html file');
                return;
            }
            res.end(data);
        });
        
        return;
    }
    
	if(req.url === '/todos' && req.method === 'POST'){
        let form = new multiparty.Form();
        form.parse(req, (err, fields, files) => {
            if(err){
                res.end('Error parsing form');
                return;
			}
			            
            fs.appendFile('./lab3/todo.txt', fields['task'] + '\n', {flag: "a"}, (err) => {
                if(err){
                    res.end('Error creating todo');
                    return;
                }
    
                res.writeHead(302, {
                    Location: '/todo/add'
                });
                res.end();
            });
        })
        return;
    }

	res.end('<h1>404: Not Found</h1>');
};

http.createServer(handler).listen(8080);